import * as IdbByDay from '../actions/idb-by-day';
import { Task } from '../shared/task';

export function reducer(state = [], action: IdbByDay.Actions): Task[] {
	switch (action.type) {

		case IdbByDay.RECIEVE_TASKS_BY_DAY:
			return action.payload;

		default:
			return state;

	}
}
