import * as IdbByPomodoro from '../actions/idb-by-pomodoro';
import { Task } from '../shared/task';

export function reducer(state = [], action: IdbByPomodoro.Actions): Task[] {
	switch (action.type) {

		case IdbByPomodoro.RECIEVE_TASKS_BY_POMODORO:
			return action.payload;

		default:
			return state;

	}
}
