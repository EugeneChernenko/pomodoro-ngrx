import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import * as fromRoot from '../../reducers/index';
import * as Idb from '../../actions/idb';
import * as IdbByPomodoro from '../../actions/idb-by-pomodoro';
import * as IdbRangeByPomodoro from '../../actions/idb-range-by-pomodoro';
import { Task } from '../../shared/task';
import { TaskStatusType } from '../../shared/types/task-status-type';

const MONTH_IN_MILLISECONDS = 30 * 24 * 60 * 60 * 1000;

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})

export class StatsComponent implements OnInit {
  private tasks$: Observable<Task[]>;
  private tasksByPomodoro$: Observable<Task[]>;
  private tasksRangeByPomodoro$: Observable<Task[]>;
  private pomodoroPerTask: number = 0;
  private pomodoroPerDay: number = 0;
  private doneTasksAmount: number = 0;
  private totalPomodoroAmount: number = 0;
  private topTasks: {} = {};
  private maxPomodoroAmount: number;
  private minValue: number = 1;
  private maxValue: number;
  private readonly topTasksAmount: number = 2;

  constructor(public store: Store<fromRoot.State>) {
    this.tasks$ = store.select(fromRoot.getTasksState);
    this.tasks$.subscribe(tasks => this.countStatistics(tasks));
    this.tasksByPomodoro$ = store.select(fromRoot.getTasksByPomodoro);
    this.tasksByPomodoro$.subscribe(tasks => this.getTopTasks(tasks));

    this.tasksRangeByPomodoro$ = store.select(fromRoot.getTasksRangeByPomodoro);
    this.tasksRangeByPomodoro$.subscribe(tasks => this.getTopTasks(tasks));
  }

  ngOnInit() {
    this.store.dispatch(new Idb.RequestTasks());
    this.store.dispatch(new IdbByPomodoro.RequestTasks());
    this.tasksByPomodoro$.subscribe(tasks => this.getMaxPomodoroAmount(tasks));
  }

  private getTopTasks(tasks: {}): any {
    this.topTasks = {};
    Object.keys(tasks).forEach(key => {
      // Skip tasks with 0 pomodoros
      if (key === '0') { return }

      this.topTasks[key] = tasks[key].slice(0, this.topTasksAmount);
    });

    return this.topTasks;
  }

  private getMaxPomodoroAmount(tasks) {
    this.maxPomodoroAmount = + Object.keys(tasks).reverse()[0] | 0;
    this.maxValue = this.maxPomodoroAmount;
  }

  private get topTasksKeys() {
    return this.topTasks ? Object.keys(this.topTasks).sort().reverse() : [];
  }

  private countStatistics(tasks: Task[]): void {
    if (tasks.length < 1) { return }
    this.countPomodoroPerTask(tasks);
    this.countPomodoroPerDay(tasks);
  }

  private isDone(task: Task): boolean {
    return task.status === TaskStatusType.DONE;
  }

  private hasPomodoro(task: Task): boolean {
    return task.pomodoro > 0;
  }

  private countPomodoroPerTask(tasks: Task[]): void {
    let doneTasks: number = this.countDoneTasks(tasks);
    let totalPomodoro: number = this.countTotalPomodoro(tasks);
    this.pomodoroPerTask = parseFloat((totalPomodoro / doneTasks).toFixed(2)) || 0;
  }

  private countTotalPomodoro(tasks: Task[]): number {
    return tasks
      .filter(task => this.isDone(task) && this.hasPomodoro(task))
      .reduce((totalPomodoroAmount, task) => totalPomodoroAmount + task.pomodoro, 0)
  }

  private countDoneTasks(tasks: Task[]): number {
    return tasks
      .filter(task => task.status === TaskStatusType.DONE)
      .length;
  }

  private countPomodoroPerDay(tasks: Task[]): void {
    let monthPomodoroTasks = this.getMonthPomodoroTasks(tasks);
    let monthPomodoroAmount = this.countMonthPomodoroAmount(monthPomodoroTasks);
    let daysAmount = this.countDaysAmount(monthPomodoroTasks);
    this.pomodoroPerDay = parseFloat((monthPomodoroAmount / daysAmount).toFixed(2));
  }

  private countMonthPomodoroAmount(tasks: Task[]): number {
    let monthPomodoroAmount: number = 0;
    tasks.forEach(task => {
      monthPomodoroAmount = monthPomodoroAmount + task.pomodoro;
    })
    return monthPomodoroAmount;
  }

  private convertToDay(val: number): number {
    return new Date(val).getDate();
  }

  private countDaysAmount(tasks: Task[]): number {
    let daysAmount: number = 1;

    for (let i = 1; i < tasks.length; i++) {
      if (this.convertToDay(tasks[i].completeDate) !== this.convertToDay(tasks[i - 1].completeDate)) {
        daysAmount++;
      }
    }
    return daysAmount;
  }

  private getMonthPomodoroTasks(tasks: Task[]): Task[] {
    let today = +new Date();
    return tasks
      .filter(task => this.hasPomodoro(task) && (today - task.completeDate) < MONTH_IN_MILLISECONDS)
  }

  private updateRange(minVal: any, maxVal: any) {
    this.minValue = parseInt(minVal);
    this.maxValue = parseInt(maxVal);
    if (this.minValue > this.maxValue) {
      this.minValue = this.maxValue - 1;
    }
    this.store.dispatch(new IdbRangeByPomodoro.RequestTasks([this.minValue, this.maxValue]));
  }

  ngOnDestroy() {
  }
}
