export const TEST_IDB_INDEXES = {
	pomodoro: {
		indexName: 'by_pomodoro',
		keyPath: 'pomodoro',
		objectParameters: {
			unique: false
		}
	},

	day: {
		indexName: 'by_day',
		keyPath: 'completeDate',
		objectParameters: {
			unique: false
		}
	}
}

export const IDB_CONFIG = {
	VERSION: 3,
	NAME: 'testDb',
	STORE_NAME: 'testStore',
	KEYPATH: 'id',
	KEYPATH_AUTO_INCREMENT: true,
	INDEXES: TEST_IDB_INDEXES,
}

