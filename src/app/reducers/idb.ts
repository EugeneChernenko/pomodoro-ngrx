import * as Idb from '../actions/idb';
import { Task } from '../shared/task';

export function reducer(state = [], action: Idb.Actions): Task[] {
	switch (action.type) {

		case Idb.RECIEVE_TASKS:
			return action.payload;

		case Idb.ADD_TASK_SUCCESS:
			return [...state, action.payload];

		case Idb.EDIT_TASK_SUCCESS:
			let index = state.findIndex((task) => task.id == action.payload.id);
			return [
				...state.slice(0, index),
				action.payload,
				...state.slice(index + 1)
			];

		case Idb.DELETE_TASK_SUCCESS:
			return state.filter(task => {
				return task.id !== action.payload.id;
			});

		default:
			return state;

	}
}
