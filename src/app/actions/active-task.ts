import { Action } from '@ngrx/store';
import { Task } from '../shared/task';

export const ACTIVATE_TASK = '[Idb] Activate task';
export const DEACTIVATE_TASK = '[Idb] Deactivate task';

export class ActivateTask implements Action {
	readonly type = ACTIVATE_TASK;

	constructor(public payload: Task) { }
}

export class DeactivateTask implements Action {
	readonly type = DEACTIVATE_TASK;

	constructor(public payload: Task) { }
}

export type Actions
	= ActivateTask
	| DeactivateTask;
