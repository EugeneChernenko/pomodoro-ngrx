import * as TaskPopup from '../actions/task-popup';

export interface TaskPopupInterface {
  open:boolean;
  templText:{} | null;
  mode:string;
};

const initialState: TaskPopupInterface = {
  open:false,
  templText:null,
  mode:null
};


export function reducer(state:TaskPopupInterface = initialState, action: TaskPopup.Actions) {
  switch (action.type) {
    case TaskPopup.SHOW_POPUP:
      return {
      	open:true,
      	templText:action.text,
      	mode:action.mode
      };

    case TaskPopup.HIDE_POPUP:
      return {
      	open:false,
      	templText:null
      }

    default:
      return state;
  }
}