import { TestBed, inject } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { TimerService } from './timer.service';
import { NotificationService } from './notification.service';
import { reducers } from '../../reducers/index';

describe('TimerService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				StoreModule.forRoot(reducers)
			],
			providers: [
				[TimerService],
				{ provide: NotificationService }
			]
		});
	});

	it('should be created', inject([TimerService], (service: TimerService) => {
		expect(service).toBeTruthy();
	}));
});
