import * as fromIdb from './idb';
import * as fromIdbByPomodoro from './idb-by-pomodoro';
import * as fromIdbRangeByPomodoro from './idb-range-by-pomodoro';
import * as fromIdbByDay from './idb-by-day';
import * as fromTimer from './timer';
import * as fromActiveTask from './active-task';
import * as fromTaskPopup from './task-popup';
import * as fromNotifications from './notifications';
import { Task } from '../shared/task';
import { Timer } from '../reducers/timer';
import { TaskPopupInterface } from './task-popup';

export const reducers = {
	timer: fromTimer.reducer,
	tasks: fromIdb.reducer,
	tasksByPomodoro: fromIdbByPomodoro.reducer,
	tasksRangeByPomodoro: fromIdbRangeByPomodoro.reducer,
	tasksByDay: fromIdbByDay.reducer,
	activeTask: fromActiveTask.reducer,
	taskPopup: fromTaskPopup.reducer,
	notifications: fromNotifications.reducer
}

export interface State {
	timer: Timer;
	tasks?: Task[];
	tasksByPomodoro?: Task[];
	tasksRangeByPomodoro?: Task[];
	tasksByDay?: Task[];
	activeTask?: Task;
	taskPopup: TaskPopupInterface;
	notifications?: string[];
}

export const getTimerState = (state: State) => state.timer;
export const getTimerMode = (state: State) => state.timer.mode;
export const getTasksState = (state: State) => state.tasks;
export const getTasksByPomodoro = (state: State) => state.tasksByPomodoro;
export const getTasksRangeByPomodoro = (state: State) => state.tasksRangeByPomodoro;
export const getTasksByDay = (state: State) => state.tasksByDay;
export const getTaskPopupState = (state: State) => state.taskPopup;
export const getActiveTaskState = (state: State) => state.activeTask;
export const getNotificationsState = (state: State) => state.notifications;
