import { Action } from '@ngrx/store';

export const SHOW_POPUP = '[Popup] Show';
export const HIDE_POPUP = '[Popup] Hide';

export class Show implements Action {
  readonly type = SHOW_POPUP;

   constructor(public text, public mode) {}
}

export class Hide implements Action {
  readonly type = HIDE_POPUP;
}

export type Actions
  = Show
  | Hide;