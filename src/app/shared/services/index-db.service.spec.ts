import { TestBed, inject } from '@angular/core/testing';

import { IndexDbService } from './index-db.service';

import { TaskStatusType } from '../types/task-status-type';
import { openDb } from './utils/utils';
import { TEST_IDB_INDEXES, IDB_CONFIG } from './utils/test-idb-config';
import { handleRequest } from '../services/utils/utils';
import { Task } from '../task';


const TEST_ID = 1;

const TEST_RANGES = [0, 2];

const TEST_TASK = {
	title: 'testString',
	status: TaskStatusType.OPEN,
	pomodoro: 0,
	completeDate: null
}

function addToDb(objectName: string, object: any): void {
	return openDb(IDB_CONFIG).then(db => {
		let request = db.transaction([objectName], 'readwrite')
			.objectStore(objectName)
			.add(object);

		return handleRequest(request);
	})
}


function createIDBMock(storeName: string): void {
	for (let i = 5; i > 0; i--) {
		addToDb(
			storeName, {
				title: 'testString',
				status: TaskStatusType.OPEN,
				pomodoro: i,
				completeDate: null
			})
	}
}

describe('IndexDbService', () => {
	let service: IndexDbService;

	beforeAll((done: DoneFn) => {
		service = new IndexDbService();
		openDb(IDB_CONFIG).
			then(createIDBMock(IDB_CONFIG.STORE_NAME));
		done();
	});

	afterAll((done: DoneFn) => {
		service.clearDb(IDB_CONFIG.NAME);
		done();
	});

	it('#read should return async items[]', (done: DoneFn) => {
		service.openDb(IDB_CONFIG);
		service.read(IDB_CONFIG.STORE_NAME).then(value => {
			expect(value).toEqual(jasmine.any(Array));
			expect(value.length).toEqual(5);
			done();
		});
	});

	it('#readByIndex should return async items[]', (done: DoneFn) => {
		service.openDb(IDB_CONFIG);
		service.readByIndex(IDB_CONFIG.STORE_NAME, IDB_CONFIG.INDEXES.pomodoro.indexName).then(value => {
			expect(value).toEqual(jasmine.any(Array));
			expect(value.length).toEqual(5);
			expect(value[0].value.pomodoro).toEqual(1);
			done();
		});
	});

	it('#readByRangeAndIndex should return async items[]', (done: DoneFn) => {
		service.openDb(IDB_CONFIG);
		service.readByRangeAndIndex(IDB_CONFIG.STORE_NAME, IDB_CONFIG.INDEXES.pomodoro.indexName, ...TEST_RANGES).then(value => {
			expect(value).toEqual(jasmine.any(Array));
			expect(value.length).toEqual(2);
			expect(value
				.map(item => item.value.pomodoro)
				.every(pomodoro => inRange(pomodoro, TEST_RANGES))
			).toBeTruthy();
			done();
		});

		function inRange(value, range) {
			return value >= range[0] && value <= range[1];
		}
	});

	it('#create should return async item id', (done: DoneFn) => {
		service.openDb(IDB_CONFIG);
		service.create(IDB_CONFIG.STORE_NAME, TEST_TASK).then(value => {
			expect(value).toEqual(jasmine.any(Object));
			expect(value.result).toEqual(jasmine.any(Number));
		});
		service.read(IDB_CONFIG.STORE_NAME).then(value => {
			expect(value.length).toEqual(6);
			done();
		});
	});

	it('#update should return async item{}', (done: DoneFn) => {
		service.openDb(IDB_CONFIG);
		service.update(IDB_CONFIG.STORE_NAME, TEST_ID, (item) => {
			item.status = TaskStatusType.DONE;
			return item;
		}).then(value => {
			expect(value).toEqual(jasmine.any(Object));
			expect(value.status).toEqual(TaskStatusType.DONE);
			done();
		});
	});

	it('#delete should return async item{}', (done: DoneFn) => {
		service.openDb(IDB_CONFIG);
		service.delete(IDB_CONFIG.STORE_NAME, TEST_ID).then(value => {
			expect(value).toEqual(jasmine.any(Object));
		});
		service.read(IDB_CONFIG.STORE_NAME).then(value => {
			expect(value.length).toEqual(5);
			done();
		});
	});

});
