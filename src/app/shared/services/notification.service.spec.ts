import { TestBed, inject } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { NotificationService } from './notification.service';
import { reducers } from '../../reducers/index';

describe('NotificationService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				StoreModule.forRoot(reducers)
			],
			providers: [NotificationService]
		});
	});

	it('should be created', inject([NotificationService], (service: NotificationService) => {
		expect(service).toBeTruthy();
	}));
});
