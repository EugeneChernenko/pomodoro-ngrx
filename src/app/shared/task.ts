export class Task {
  id: number;
  title: string;
  status: string;
  pomodoro: number;
  completeDate: number;
}
