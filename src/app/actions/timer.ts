import { Action } from '@ngrx/store';

export const START_TIMER = '[Timer] Start';
export const TIMER_TICK = '[Timer] Tick';
export const STOP_TIMER = '[Timer] Stop';
export const FINISH_TIMER = '[Timer] Finish';

export class Start implements Action {
	readonly type = START_TIMER;
}

export class Tick implements Action {
	readonly type = TIMER_TICK;

	constructor(public payload: number) { }
}

export class Stop implements Action {
	readonly type = STOP_TIMER;
}

export class Finish implements Action {
	readonly type = FINISH_TIMER;

	constructor(public payload: Object) { }
}

export type Actions
	= Start
	| Tick
	| Stop
	| Finish;