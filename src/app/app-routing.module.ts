import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimerComponent } from './components/timer/timer.component';
import { StatsComponent } from './components/stats/stats.component';
import { HistoryComponent } from './components/history/history.component';

const routes: Routes = [
	{ path: '', redirectTo: '/timer', pathMatch: 'full' },
	{ path: 'timer', component: TimerComponent },
	{ path: 'stats', component: StatsComponent },
	{ path: 'history', component: HistoryComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
