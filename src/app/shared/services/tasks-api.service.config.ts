
export const TASK_IDB_INDEXES = {
  pomodoro: {
    indexName: 'by_pomodoro',
    keyPath: 'pomodoro',
    objectParameters: {
      unique: false
    }
  },

  day: {
    indexName: 'by_day',
    keyPath: 'completeDate',
    objectParameters: {
      unique: false
    }
  }
}

export const IDB_CONFIG = {
  VERSION: 3,
  NAME: 'TaskDb',
  STORE_NAME: 'tasks',
  KEYPATH: 'id',
  KEYPATH_AUTO_INCREMENT: true,
  INDEXES: TASK_IDB_INDEXES,
}
