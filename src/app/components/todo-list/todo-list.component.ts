import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';
import * as Idb from '../../actions/idb';
import * as TaskPopup from '../../actions/task-popup';
import { TaskPopupInterface } from '../../reducers/task-popup';
import * as fromRoot from '../../reducers/index';

import { Task } from '../../shared/task';
import { TaskStatusType } from '../../shared/types/task-status-type';

import { TaskService } from '../../shared/services/task.service';
import { TasksApiService } from '../../shared/services/tasks-api.service';
import { NotificationService } from '../../shared/services/notification.service';
import { addTaskPopupText, editTaskPopupText, TaskPopupModes } from './popup-config'

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})

export class TodoListComponent implements OnInit {
  private addTaskPopupText = addTaskPopupText;
  private editTaskPopupText = editTaskPopupText;
  private TaskPopupModes = TaskPopupModes;
  private taskTitle: string = '';
  private targetTask: Task;
  private TaskStatusType = TaskStatusType;
  private tasks$: Observable<Task[]>;
  private taskPopup$: Observable<TaskPopupInterface>;

  constructor(
    private notificationService: NotificationService,
    private taskService: TaskService,
    private tasksApiService: TasksApiService,
    public store: Store<fromRoot.State>
  ) {
    this.tasks$ = store.select(fromRoot.getTasksState);
    this.taskPopup$ = store.select(fromRoot.getTaskPopupState);
  }

  ngOnInit() {
    this.store.dispatch(new Idb.RequestTasks());
  }

  private toogleTaskPopup(text?, mode?, task?): void {
    this.targetTask = task;
    let templText;
    templText = text;
    templText ?
      this.store.dispatch(new TaskPopup.Show(templText, mode)) :
      this.store.dispatch(new TaskPopup.Hide());
  }

  private toggleTask(task: Task): void {
    this.taskService.toggleTask(task);
  }

  private addTask(taskTitle: string): any {
    if (!this.isEmpty(taskTitle)) { return }
    let newTask = {
      title: taskTitle.toString(),
      status: this.TaskStatusType.OPEN,
      pomodoro: 0,
      completeDate: null
    }
    this.store.dispatch(new Idb.AddTask(newTask));
    this.taskTitle = '';
    this.store.dispatch(new TaskPopup.Hide());
  }

  private editTask(taskTitle: string): any {
    if (!this.isEmpty(taskTitle)) { return }

    this.store.dispatch(new Idb.EditTask({
      property: 'title',
      value: taskTitle,
      task: this.targetTask
    }));

    this.taskTitle = '';
    this.store.dispatch(new TaskPopup.Hide())
  }


  private deleteTask(removedTask: Task): void {
    if (this.isActive(removedTask)) {
      this.notify('Error! You can\'t delete active task. Please stop the task before deleting!');
      return
    }
    this.store.dispatch(new Idb.DeleteTask(removedTask));
  }


  private closeTask(task: Task) {
    if (this.isActive(task)) {
      this.notify('Error! You can\'t close active task. Please stop the task before closing!');
      return
    }

    if (task.status == this.TaskStatusType.DONE) {
      this.reopenTask(task);
    } else {
      this.store.dispatch(new Idb.EditTask({
        property: 'status',
        value: this.TaskStatusType.DONE,
        task: task
      }));

      this.store.dispatch(new Idb.EditTask({
        property: 'completeDate',
        value: new Date().getTime(),
        task: task
      }));
    }
  }

  private reopenTask(task: Task) {
    task.status = this.TaskStatusType.OPEN;
    this.store.dispatch(new Idb.EditTask({
      property: 'status',
      value: task.status,
      task: task
    }));
  }

  private isEmpty(input: any): boolean {
    return input = input.trim();
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }

  private isActive(task: Task): boolean {
    return this.taskService.activeTask && task.id == this.taskService.activeTask.id;
  }

  ngOnDestroy() { }
}
