import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { reducers } from '../../reducers/index';
import { TimerComponent } from './timer.component';
import { TimerService } from '../../shared/services/timer.service';
import { TaskService } from '../../shared/services/task.service';
import { NotificationService } from '../../shared/services/notification.service';
import { TaskStatusType } from '../../shared/types/task-status-type';
import { SecondsToTimePipe } from '../../shared/pipes/seconds-to-time.pipe';
import { NO_ERRORS_SCHEMA } from '@angular/core';

const DEFAULT_TIMER_TIME = '00:00';
const TEST_TIMER_TIME = '04:55';
const TEST_TIMER_TIME_2 = '04:50';

const TEST_TASK = {
  id: 1,
  title: 'testString',
  status: TaskStatusType.OPEN,
  pomodoro: 0,
  completeDate: null
}

let taskServiceStub = {
  activeTask: TEST_TASK
};

describe('TimerComponent', () => {
  let component: TimerComponent;
  let fixture: ComponentFixture<TimerComponent>;
  let debugElement: DebugElement;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(reducers)
      ],
      declarations: [TimerComponent, SecondsToTimePipe],
      providers: [
        { provide: TaskService, useValue: taskServiceStub },
        { provide: NotificationService },
        TimerService,
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimerComponent);
    component = fixture.componentInstance;
    let taskService = TestBed.get(TaskService);
    let timerService = TestBed.get(TimerService);
    let notificationService = TestBed.get(NotificationService);

    debugElement = fixture.debugElement.query(By.css('.timer-digits'));

    if (debugElement) {
      element = debugElement.nativeElement;
    }

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a default timer time', () => {
    expect(element.textContent).toContain(DEFAULT_TIMER_TIME);
  });

  it('#click on start - timer should display a proper timer time after 5 seconds from starting ', fakeAsync(() => {
    let startBtn = fixture.debugElement.query(By.css('.btn-start-pomodoro'));
    startBtn.triggerEventHandler('click', null);
    tick(5000);
    fixture.detectChanges();
    expect(element.textContent).toContain(TEST_TIMER_TIME);
    component.stopTimer();
  }));

  it('#click on stop - timer should display a default timer time', fakeAsync(() => {
    let startBtn = fixture.debugElement.query(By.css('.btn-start-pomodoro'));
    startBtn.triggerEventHandler('click', null);
    fixture.detectChanges();

    let stopBtn = fixture.debugElement.query(By.css('.btn-stop'));
    stopBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
  }));

  it('#click on pause - timer should display a proper timer time after pausing and the same time after 5s', fakeAsync(() => {
    let startBtn = fixture.debugElement.query(By.css('.btn-start-pomodoro'));
    startBtn.triggerEventHandler('click', null);
    tick(5000);
    fixture.detectChanges();

    let pauseBtn = fixture.debugElement.query(By.css('.btn-pause'));
    pauseBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    tick(5000);
    fixture.detectChanges();
    expect(element.textContent).toContain(TEST_TIMER_TIME);
    component.stopTimer();
  }));


  it('#click on resume - timer should display a proper timer time after resuming', fakeAsync(() => {
    let startBtn = fixture.debugElement.query(By.css('.btn-start-pomodoro'));
    startBtn.triggerEventHandler('click', null);
    tick(5000);
    fixture.detectChanges();

    let pauseBtn = fixture.debugElement.query(By.css('.btn-pause'));
    pauseBtn.triggerEventHandler('click', null);
    fixture.detectChanges();
    let resumeBtn = fixture.debugElement.query(By.css('.btn-resume'));
    resumeBtn.triggerEventHandler('click', null);

    tick(5000);
    fixture.detectChanges();
    expect(element.textContent).toContain(TEST_TIMER_TIME_2);
    component.stopTimer();
  }));

});
