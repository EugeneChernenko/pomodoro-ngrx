import { Action } from '@ngrx/store';
import { Task } from '../shared/task';

export const REQUEST_TASKS_BY_DAY = '[Idb] Request tasks by day';
export const RECIEVE_TASKS_BY_DAY = '[Idb] Recieve tasks by day';
export const REQUEST_FAILED = '[Idb] Request failed';

export class RequestTasks implements Action {
	readonly type = REQUEST_TASKS_BY_DAY;
}

export class RecieveTasks implements Action {
	readonly type = RECIEVE_TASKS_BY_DAY;

	constructor(public payload: Task[]) { }
}

export class RequestFailed implements Action {
	readonly type = REQUEST_FAILED;

	constructor(public error: any) { }
}

export type Actions =
	| RequestTasks
	| RecieveTasks
	| RequestFailed;
