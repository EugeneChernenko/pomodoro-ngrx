import * as ActiveTask from '../actions/active-task';
import { Task } from '../shared/task';


export function reducer(state: any, action: ActiveTask.Actions) {
	switch (action.type) {

		case ActiveTask.ACTIVATE_TASK:
			return action.payload

		case ActiveTask.DEACTIVATE_TASK:
			return null

		default:
			return state;
	}
}