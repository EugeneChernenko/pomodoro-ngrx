import { Injectable } from '@angular/core';

import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../reducers/index';
import * as Notifications from '../../actions/notifications';

@Injectable()
export class NotificationService {
  private sound;

  constructor(public store: Store<fromRoot.State>) {
    this.sound = new Audio();
    this.sound.src = "../assets/sounds/notification.mp3";
    this.sound.load();
  }

  public add(message: string): void {
    this.store.dispatch(new Notifications.AddMessage(message));
    this.playSound();
    this.remove();
  }

  private remove(): void {
    setTimeout(() => this.store.dispatch(new Notifications.RemoveMessage()), 6000);
  }

  private playSound(): void {
    this.sound.play();
  }

}
