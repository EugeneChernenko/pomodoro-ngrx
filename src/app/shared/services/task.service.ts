import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../../reducers/index';
import * as Idb from '../../actions/idb';
import * as ActiveTask from '../../actions/active-task';
import * as Timer from '../../actions/timer';

import { Task } from '../task';
import { TimerType } from '../types/timer-type';
import { TaskStatusType } from '../types/task-status-type';

import { TimerService } from './timer.service';
import { NotificationService } from './notification.service';

@Injectable()
export class TaskService {
  private TimerType = TimerType;
  private TaskStatusType = TaskStatusType;
  private completedPomodoroSubscription: Subscription;
  private tasks$: Observable<Task[]>;
  private timerFinished$: Observable<Object>;
  public activeTask: Task;

  constructor(
    private timerService: TimerService,
    private notificationService: NotificationService,
    public store: Store<fromRoot.State>
  ) {
    this.timerFinished$ = store.select(fromRoot.getTimerMode);
    this.timerFinished$.subscribe(payload => this.publishCompletedTimer(payload));
    this.tasks$ = store.select(fromRoot.getTasksState);
    this.tasks$.subscribe(tasks => this.checkActiveTask(tasks));

  }

  ngOnInit() {
  }

  private checkActiveTask(tasks: Task[]): void {
    if (!tasks.length) { return };
    let task = tasks.find(task => {
      return task.status == this.TaskStatusType.IN_PROGRESS;
    });
    this.setActiveTask(task);
  }

  private setActiveTask(task: Task): void {
    if (task) {
      this.activeTask = task;
      this.store.dispatch(new ActiveTask.ActivateTask(task));
    } else {
      this.activeTask = null;
    }
  }

  private publishCompletedTimer(payload): void {
    if (!payload) { return };
    if (payload.type == this.TimerType.POMODORO) {
      this.addCompletedPomodoro();
      this.notify('Pomodoro completed!');
    } else {
      this.notify('Break completed!');
    }
  }

  private addCompletedPomodoro(): void {
    this.activeTask.pomodoro++;
    this.store.dispatch(new Idb.EditTask({
      property: this.TimerType.POMODORO,
      value: this.activeTask.pomodoro,
      task: this.activeTask
    }));
  }

  public toggleTask(task: Task): any {
    if (this.timerService.isStarted()) {
      this.notify('Error! Please stop your timer or wait until it is finished!')
    } else {
      if (task.status == this.TaskStatusType.DONE) {
        this.notify('Error! You can\'t start working on closed task. Please reopen it before starting!');
        return
      }
      if (task.status !== this.TaskStatusType.IN_PROGRESS) {
        this.deactivatePreviousTask();
        this.activateCurrentTask(task);
      } else {
        this.deactivatePreviousTask();
      }
      return task;
    }
  }

  private deactivatePreviousTask(): void {
    if (this.activeTask) {
      this.activeTask.status = this.TaskStatusType.OPEN;
      this.store.dispatch(new Idb.EditTask({
        property: 'status',
        value: this.TaskStatusType.OPEN,
        task: this.activeTask
      }));
      this.store.dispatch(new ActiveTask.DeactivateTask(this.activeTask));
    }
  }

  private activateCurrentTask(task: Task): void {
    task.status = this.TaskStatusType.IN_PROGRESS;
    this.store.dispatch(new Idb.EditTask({
      property: 'status',
      value: task.status,
      task: task
    }));
    this.store.dispatch(new ActiveTask.ActivateTask(task));
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }
}
