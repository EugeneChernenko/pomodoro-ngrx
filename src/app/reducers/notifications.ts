import * as Notifications from '../actions/notifications';

export function reducer(state = [], action: Notifications.Actions) {
	switch (action.type) {

		case Notifications.ADD_MESSAGE:
			return [action.payload, ...state];

		case Notifications.REMOVE_MESSAGE:
			return state.slice(0, (state.length - 1));

		default:
			return state;
	}
}