export function openDb(config): Promise<any> {
	return new Promise((resolve, reject) => {
		let request = window.indexedDB.open(config.NAME, config.VERSION);
		handleRequest(request)
			.then((data: any) => {
				console.log('test iDb opened successfully!');
				resolve(data.result);
			})
			.catch(() => {
				console.error('Unable to open the test iDb!');
			})

		request.onupgradeneeded = event => {
			let target: any = event.target;
			let db = target.result;
			let objectStore = db.createObjectStore(
				config.STORE_NAME,
				{ keyPath: config.KEYPATH, autoIncrement: config.KEYPATH_AUTO_INCREMENT || false }
			);

			if (config.INDEXES) {
				Object.keys(config.INDEXES).forEach(index => {
					objectStore.createIndex(
						config.INDEXES[index].indexName,
						config.INDEXES[index].keyPath,
						config.INDEXES[index].objectParameters || {}
					);
				})
			}
		}
	})
}

export function handleRequest(request) {
	return new Promise((resolve, reject) => {
		request.onsuccess = event => {
			resolve(event.target);
		}
		request.onerror = event => {
			reject(event.target);
		};
	})
};

export function handleCollectionRequest(request) {
	let result = [];
	return new Promise((resolve, reject) => {
		request.onsuccess = event => {
			let cursor = event.target.result;

			if (!cursor) {
				return resolve(result);
			}

			result.push({ key: cursor.primaryKey, value: cursor.value });
			cursor.continue();
		}

		request.onerror = event => {
			reject(event.target);
		}
	})
};