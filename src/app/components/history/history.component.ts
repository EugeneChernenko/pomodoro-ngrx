import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';

import { Task } from '../../shared/task';
import * as fromRoot from '../../reducers/index';
import * as IdbByDay from '../../actions/idb-by-day';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})

export class HistoryComponent implements OnInit {
  private tasksByDay$: Observable<Task[]>;
  private tasksByDay: {} = {};
  private totalPomodoro: number = 0;

  constructor(public store: Store<fromRoot.State>) {
    this.tasksByDay$ = store.select(fromRoot.getTasksByDay);
    this.tasksByDay$.subscribe(tasks => this.tasksByDay = tasks);
  }

  ngOnInit() {
    this.store.dispatch(new IdbByDay.RequestTasks());
  }

  private get historyKeys() {
    return this.tasksByDay ? Object.keys(this.tasksByDay).sort(this.compareByDay.bind(this)).reverse() : []
  }

  private countTotalDailyPomodoro(day: {}): number {
    return Object.keys(day)
      .reduce((totalPomodoro, task) => totalPomodoro + day[task].pomodoro, 0)
  }

  private compareByDay(a, b): number {
    return this.tasksByDay[a][0].completeDate - this.tasksByDay[b][0].completeDate;
  }

  private isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }
}
