import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from "rxjs";
import { Timer } from '../../reducers/timer';
import * as fromRoot from '../../reducers';

import { TimerType } from '../../shared/types/timer-type';
import { TimerService } from '../../shared/services/timer.service';
import { TaskService } from '../../shared/services/task.service';
import { NotificationService } from '../../shared/services/notification.service';


@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})

export class TimerComponent implements OnInit {
  private TimerType = TimerType;
  private tickSubscription: Subscription;
  private timeleft: number = 0;
  private timer$: Observable<Timer>;

  constructor(
    private timerService: TimerService,
    private taskService: TaskService,
    private notificationService: NotificationService,
    private store: Store<fromRoot.State>
  ) {
    this.timer$ = store.select(fromRoot.getTimerState);
    this.timer$.subscribe(payload => this.onTimerTick(payload));
  }

  ngOnInit() {

  }

  private onTimerTick(timer) {
    this.timeleft = timer.timeleft;
  }

  startTimer(mins: number, payload?: Object): void {
    if (this.taskService.activeTask) {
      this.timerService.start(mins, payload);
    } else {
      this.notify('You don\'t have any active tasks! Please start the task before your timer!');
    }
  }

  pauseTimer(): void {
    this.timerService.pause();
  }

  stopTimer(): void {
    this.timerService.stop();
  }

  resumeTimer(): void {
    if (this.isPaused()) {
      this.timerService.resume();
    }
  }

  private isPaused(): boolean {
    return this.timerService.isPaused();
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }

  ngOnDestroy() {
  }

}
