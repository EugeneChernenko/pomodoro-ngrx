import { Component, OnInit, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../reducers/index';

import { NotificationService } from '../../shared/services/notification.service';
import * as Notifications from '../../actions/notifications';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})

export class NotificationsComponent implements OnInit {
  private notifications$: Observable<string[]>;
  private notifications: string[] = [];

  constructor(
    private notificationService: NotificationService,
    public store: Store<fromRoot.State>
  ) {
    this.notifications$ = store.select(fromRoot.getNotificationsState);
    this.notifications$.subscribe(notifications => this.notifications = notifications);
  }

  ngOnInit() { }

  ngOnDestroy() { }
}
