import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TimerComponent } from './components/timer/timer.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TaskService } from './shared/services/task.service';
import { TimerService } from './shared/services/timer.service';
import { SecondsToTimePipe } from './shared/pipes/seconds-to-time.pipe';
import { MillisecondsToDatePipe } from './shared/pipes/milliseconds-to-date.pipe';
import { ActiveTaskComponent } from './components/active-task/active-task.component';
import { StatsComponent } from './components/stats/stats.component';
import { NotificationService } from './shared/services/notification.service';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { TasksApiService } from './shared/services/tasks-api.service';
import { HistoryComponent } from './components/history/history.component';
import { IdbEffects } from "./effects/idb-effects";
import { reducers } from './reducers/index';
import { IndexDbService } from './shared/services/index-db.service';

@NgModule({
  declarations: [
    AppComponent,
    TimerComponent,
    TodoListComponent,
    SecondsToTimePipe,
    MillisecondsToDatePipe,
    ActiveTaskComponent,
    StatsComponent,
    NotificationsComponent,
    HistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([IdbEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    })
  ],
  providers: [TaskService, TimerService, NotificationService, TasksApiService, IndexDbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
