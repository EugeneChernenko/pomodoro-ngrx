import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { Subject } from 'rxjs/Subject';

import * as Timer from '../../actions/timer';
import * as fromRoot from '../../reducers/index';
import { NotificationService } from './notification.service';

let toSeconds = function(mins: number): number {
  return mins * 60;
};

let toMinutes = function(seconds: number): number {
  return seconds / 60;
};

@Injectable()
export class TimerService {
  private TIMER_INITIAL_POINT = 0;
  private TIMER_STEP = 1000;
  private tick = new Subject<any>();

  private subscription: Subscription;
  private timeLeft: number = 0;
  private paused: boolean = false;
  private finished$: Observable<boolean>;
  public tick$ = this.tick.asObservable();


  constructor(
    private notificationService: NotificationService,
    public store: Store<fromRoot.State>
  ) { }

  public start(mins: number, payload?: Object): void {
    if (this.subscription) {
      this.unsubscribe();
    }
    this.store.dispatch(new Timer.Start());
    let timer = TimerObservable.create(this.TIMER_INITIAL_POINT, this.TIMER_STEP);
    this.subscription = timer.subscribe(count => {
      this.timeLeft = toSeconds(mins) - count;
      if (this.timeLeft <= 0) {
        this.store.dispatch(new Timer.Finish(payload));
        this.unsubscribe();
      }
      this.store.dispatch(new Timer.Tick(this.timeLeft));
    })
  }

  public pause(): void {
    this.paused = true;
    this.unsubscribe();
  }

  public stop(): void {
    this.timeLeft = 0;
    this.paused = false;
    this.store.dispatch(new Timer.Stop());
    this.unsubscribe();
  }

  public resume(): void {
    this.paused = false;
    let mins = toMinutes(this.timeLeft);
    return this.start(mins);
  }

  public isPaused(): boolean {
    return this.paused;
  }

  public isStarted(): boolean {
    return this.timeLeft > 0;
  }

  private unsubscribe(): void {
    this.subscription.unsubscribe();
  }

  private notify(message: string) {
    this.notificationService.add(message);
  }
}
