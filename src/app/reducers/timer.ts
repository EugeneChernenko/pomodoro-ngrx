import * as Timer from '../actions/timer';

export interface Timer {
	active: boolean;
	timeleft: number;
	mode: string | null;
};

const initialState: Timer = {
	active: false,
	timeleft: 0,
	mode: null
};


export function reducer(state: Timer = initialState, action: Timer.Actions) {
	switch (action.type) {
		case Timer.START_TIMER:
			return {
				...state,
				active: true,
			}

		case Timer.TIMER_TICK:
			return {
				...state,
				active: true,
				timeleft: action.payload
			}

		case Timer.STOP_TIMER:
			return {
				...state,
				active: false,
				timeleft: 0
			}

		case Timer.FINISH_TIMER:
			return {
				...state,
				active: false,
				timeleft: 0,
				mode: action.payload
			}

		default:
			return state;
	}
}