import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { of } from 'rxjs/observable/of';
import { IndexDbService as indexDb } from './index-db.service';
import { Task } from '../task';
import { TEST_IDB_INDEXES, IDB_CONFIG } from './utils/test-idb-config';

@Injectable()
export class TasksApiService {

  constructor(private indexDb: indexDb, ) {
    this.indexDb.openDb(IDB_CONFIG);
  }

  public getAll(): Observable<Task[]> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .read(IDB_CONFIG.STORE_NAME)
        .then((result: Array<any>) => {
          let taskList = result.reduce((tasks, item) => {
            let task = item.value;
            tasks.push(task);

            return tasks;
          }, []);
          return resolve(taskList);
        })
    }))
  }

  public getPomodoroRange(props): Observable<Task[]> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .readByRangeAndIndex(IDB_CONFIG.STORE_NAME, IDB_CONFIG.INDEXES.pomodoro.indexName, ...props)
        .then((result: Array<any>) => {
          let samePomodoroTasks = this.sortByPomodoro(result);
          return resolve(samePomodoroTasks);
        })
    }))
  }

  public getByPomodoro(): Observable<Task[]> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .readByIndex(IDB_CONFIG.STORE_NAME, IDB_CONFIG.INDEXES.pomodoro.indexName)
        .then((result: Array<any>) => {
          let samePomodoroTasks = this.sortByPomodoro(result);
          return resolve(samePomodoroTasks);
        })
    }))
  }

  private sortByPomodoro(list: any[]): any[] {
    return list.reduce((tasks, item) => {
      let [pomodoro, task] = [item.value.pomodoro, item.value];
      tasks[pomodoro] = tasks[pomodoro] || [];
      tasks[pomodoro].push(task);

      return tasks;
    }, {});
  }

  public getByDay(): Observable<Task[]> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .readByIndex(IDB_CONFIG.STORE_NAME, IDB_CONFIG.INDEXES.day.indexName)
        .then((result: Array<any>) => {
          let keyToDay = key => new Date(+key).getDate();
          let tasksByDay = result.reduce((tasks, item) => {
            let [day, task] = [keyToDay(item.value.completeDate), item.value];
            tasks[day] = tasks[day] || [];
            tasks[day].push(task);

            return tasks;
          }, {});

          return resolve(tasksByDay);
        })
    }))
  }

  public add(task: any): Observable<Task> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .create(IDB_CONFIG.STORE_NAME, task)
        .then((data: any) => {
          console.log('Task has been added to your database.');
          task.id = data.result;
          return resolve(task);
        })
        .catch(() => {
          console.error('Unable to add task');
        })
    }))
  }

  public edit(props: any): Observable<Task> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .update(IDB_CONFIG.STORE_NAME, props.task.id, (item) => {
          item[props.property] = props.value;
          return item;
        })
        .then((task) => {
          console.log('Task has been edited in your database.');
          resolve(task);
        })
        .catch(() => {
          console.log('Unable to edit the task!');
        })
    }))
  }

  public delete(task: any): Observable<Task> {
    return fromPromise(new Promise((resolve, reject) => {
      this.indexDb
        .delete(IDB_CONFIG.STORE_NAME, task.id)
        .then(() => {
          console.log('Task has been deleted from your database.');
          resolve(task);
        })
        .catch(() => {
          console.error('Unable to delete the task!');
        })
    }))
  }
}

