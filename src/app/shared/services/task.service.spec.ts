import { TestBed, inject } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';

import { TaskService } from './task.service';
import { TimerService } from './timer.service';
import { NotificationService } from './notification.service';
import { reducers } from '../../reducers/index';

describe('TaskService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				StoreModule.forRoot(reducers)
			],
			providers: [
				TaskService,
				{ provide: TimerService },
				{ provide: NotificationService }
			]
		});
	});

	it('should be created', inject([TaskService], (service: TaskService) => {
		expect(service).toBeTruthy();
	}));
});
