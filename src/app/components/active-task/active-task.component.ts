import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Store, select } from '@ngrx/store';

import { TaskStatusType } from '../../shared/types/task-status-type';
import { Task } from '../../shared/task';

import * as fromRoot from '../../reducers/index';

@Component({
  selector: 'app-active-task',
  templateUrl: './active-task.component.html',
  styleUrls: ['./active-task.component.css']
})

export class ActiveTaskComponent {
  private activeTask$;
  private activeTask: Task;

  constructor(public store: Store<fromRoot.State>) {
    this.activeTask$ = store.select(fromRoot.getActiveTaskState);
    this.activeTask$.subscribe(task => this.activeTask = task)
  }

  ngOnInit() {

  }


  ngOnDestroy() {

  }
}
