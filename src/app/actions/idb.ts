import { Action } from '@ngrx/store';
import { Task } from '../shared/task';

export const REQUEST_TASKS = '[Idb] Request tasks';
export const RECIEVE_TASKS = '[Idb] Recieve tasks';
export const ADD_TASK = '[Idb] Add task';
export const ADD_TASK_SUCCESS = '[Idb] Add task success';
export const EDIT_TASK = '[Idb] Edit task';
export const EDIT_TASK_SUCCESS = '[Idb] Edit task success';
export const DELETE_TASK = '[Idb] Delete task';
export const DELETE_TASK_SUCCESS = '[Idb] Delete task success';
export const CLOSE_TASK = '[Idb] Close task';
export const CLOSE_TASK_SUCCESS = '[Idb] Close task success';
export const REOPEN_TASK = '[Idb] Reopen task';
export const REOPEN_TASK_SUCCESS = '[Idb] Reopen task success';
export const REQUEST_FAILED = '[Idb] Request failed';

export interface IdbAction extends Action {
  payload?: any;
}

export class RequestTasks implements IdbAction {
  readonly type = REQUEST_TASKS;
}

export class RecieveTasks implements IdbAction {
  readonly type = RECIEVE_TASKS;

  constructor(public payload: Task[]) { }
}

export class AddTask implements IdbAction {
  readonly type = ADD_TASK;

  constructor(public payload: any) { }
}

export class AddTaskSuccess implements IdbAction {
  readonly type = ADD_TASK_SUCCESS;

  constructor(public payload: any) { }
}

export class EditTask implements IdbAction {
  readonly type = EDIT_TASK;

  constructor(public payload: any) { }
}

export class EditTaskSuccess implements IdbAction {
  readonly type = EDIT_TASK_SUCCESS;

  constructor(public payload) { }
}

export class DeleteTask implements IdbAction {
  readonly type = DELETE_TASK;

  constructor(public payload: any) { }
}

export class DeleteTaskSuccess implements IdbAction {
  readonly type = DELETE_TASK_SUCCESS;

  constructor(public payload: any) { }
}

export class CloseTask implements IdbAction {
  readonly type = CLOSE_TASK;

  constructor(public payload: any) { }
}

export class CloseTaskSuccess implements IdbAction {
  readonly type = CLOSE_TASK_SUCCESS;

  constructor(public payload: any) { }
}

export class ReopenTask implements IdbAction {
  readonly type = REOPEN_TASK;

  constructor(public payload: any) { }
}

export class ReopenTaskSuccess implements IdbAction {
  readonly type = REOPEN_TASK_SUCCESS;

  constructor(public payload: any) { }
}

export class RequestFailed implements IdbAction {
  readonly type = REQUEST_FAILED;

  constructor(public error: any) { }
}

export type Actions =
  | RequestTasks
  | RecieveTasks
  | AddTask
  | AddTaskSuccess
  | EditTask
  | EditTaskSuccess
  | DeleteTask
  | DeleteTaskSuccess
  | CloseTask
  | CloseTaskSuccess
  | ReopenTask
  | ReopenTaskSuccess
  | RequestFailed;
