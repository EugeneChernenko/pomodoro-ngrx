import * as IdbRangeByPomodoro from '../actions/idb-range-by-pomodoro';
import { Task } from '../shared/task';

export function reducer(state = [], action: IdbRangeByPomodoro.Actions): Task[] {
	switch (action.type) {

		case IdbRangeByPomodoro.RECIEVE_TASKS_RANGE_BY_POMODORO:
			return action.payload;

		default:
			return state;

	}
}
