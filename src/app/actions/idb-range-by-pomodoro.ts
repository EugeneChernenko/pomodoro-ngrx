import { Action } from '@ngrx/store';
import { Task } from '../shared/task';

export const REQUEST_TASKS_RANGE_BY_POMODORO = '[Idb] Request tasks range by pomodoro';
export const RECIEVE_TASKS_RANGE_BY_POMODORO = '[Idb] Recieve tasks range by pomodoro';
export const REQUEST_FAILED = '[Idb] Request failed';

export interface IdbAction extends Action {
	payload?: any;
}

export class RequestTasks implements IdbAction {
	readonly type = REQUEST_TASKS_RANGE_BY_POMODORO;

	constructor(public payload: {}) { }
}

export class RecieveTasks implements IdbAction {
	readonly type = RECIEVE_TASKS_RANGE_BY_POMODORO;

	constructor(public payload: Task[]) { }
}


export class RequestFailed implements IdbAction {
	readonly type = REQUEST_FAILED;

	constructor(public error: any) { }
}

export type Actions =
	| RequestTasks
	| RecieveTasks
	| RequestFailed;
