import { Injectable } from "@angular/core";
import { Store, Action } from "@ngrx/store";
import { Effect, Actions } from "@ngrx/effects";
import { Observable } from "rxjs/Observable";
import { of } from 'rxjs/observable/of';
import { switchMap, mergeMap, map, catchError } from 'rxjs/operators';

import { TasksApiService } from "../shared/services/tasks-api.service";
import * as Idb from '../actions/idb';
import * as IdbByPomodoro from '../actions/idb-by-pomodoro';
import * as IdbRangeByPomodoro from '../actions/idb-range-by-pomodoro';
import * as IdbByDay from '../actions/idb-by-day';


@Injectable()
export class IdbEffects {

  @Effect()
  requestTasks$: Observable<Idb.IdbAction> = this.actions$
    .ofType(Idb.REQUEST_TASKS)
    .pipe(
    switchMap(actions => this.tasksApiServ.getAll()),
    switchMap(tasks => of(new Idb.RecieveTasks(tasks))),
    catchError(err => {
      console.log('err', err);
      return of(new Idb.RequestFailed(err))
    })
    )

  @Effect()
  requestTasksByPomodoro$: Observable<Action> = this.actions$
    .ofType(IdbByPomodoro.REQUEST_TASKS_BY_POMODORO)
    .pipe(
    switchMap(actions => this.tasksApiServ.getByPomodoro()),
    switchMap(tasks => of(new IdbByPomodoro.RecieveTasks(tasks))),
    catchError(err => {
      console.log('err', err);
      return of(new IdbByPomodoro.RequestFailed(err))
    })
    )

  @Effect()
  requestTasksRangeByPomodoro$: Observable<IdbRangeByPomodoro.IdbAction> = this.actions$
    .ofType(IdbRangeByPomodoro.REQUEST_TASKS_RANGE_BY_POMODORO)
    .pipe(
    switchMap((action: IdbRangeByPomodoro.IdbAction) => this.tasksApiServ.getPomodoroRange(action.payload)),
    switchMap(tasks => of(new IdbRangeByPomodoro.RecieveTasks(tasks))),
    catchError(err => {
      console.log('err', err);
      return of(new IdbByPomodoro.RequestFailed(err))
    })
    )

  @Effect()
  requestTasksByDay$: Observable<Action> = this.actions$
    .ofType(IdbByDay.REQUEST_TASKS_BY_DAY)
    .pipe(
    switchMap(actions => this.tasksApiServ.getByDay()),
    switchMap(tasks => of(new IdbByDay.RecieveTasks(tasks))),
    catchError(err => {
      console.log('err', err);
      return of(new IdbByDay.RequestFailed(err))
    })
    )

  @Effect()
  addTask$: Observable<Idb.IdbAction> = this.actions$
    .ofType(Idb.ADD_TASK)
    .pipe(
    switchMap((action: Idb.IdbAction) => this.tasksApiServ.add(action.payload)),
    switchMap(task => of(new Idb.AddTaskSuccess(task))),
    catchError(err => {
      console.log('err', err);
      return of(new Idb.RequestFailed(err))
    })
    )

  @Effect()
  editTask$: Observable<Idb.IdbAction> = this.actions$
    .ofType(Idb.EDIT_TASK)
    .pipe(
    mergeMap((action: Idb.IdbAction) => this.tasksApiServ.edit(action.payload)),
    mergeMap(task => of(new Idb.EditTaskSuccess(task))),
    catchError(err => {
      console.log('err', err);
      return of(new Idb.RequestFailed(err))
    })
    )

  @Effect()
  deleteTask$: Observable<Idb.IdbAction> = this.actions$
    .ofType(Idb.DELETE_TASK)
    .pipe(
    switchMap((action: Idb.IdbAction) => this.tasksApiServ.delete(action.payload)),
    switchMap(task => of(new Idb.DeleteTaskSuccess(task))),
    catchError(err => {
      console.log('err', err);
      return of(new Idb.RequestFailed(err))
    })
    )

  constructor(
    private actions$: Actions,
    private tasksApiServ: TasksApiService
  ) { }
}
