import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/observable/fromPromise';

interface IDBIndex {
  indexName: string,
  keyPath: string,
  objectParameters?: {
    unique?: boolean,
    multiEntry?: boolean,
    locale?: any
  }
}

interface Config {
  VERSION: number,
  NAME: string,
  STORE_NAME: string,
  KEYPATH: string,
  KEYPATH_AUTO_INCREMENT?: boolean,
  INDEXES?: {
    [key: string]: IDBIndex
  }
}

let handleRequest = request => {
  return new Promise((resolve, reject) => {
    request.onsuccess = event => {
      resolve(event.target);
    }
    request.onerror = event => {
      reject(event.target);
    };
  })
};

let handleCollectionRequest = request => {
  let result = [];
  return new Promise((resolve, reject) => {
    request.onsuccess = event => {
      let cursor = event.target.result;

      if (!cursor) {
        return resolve(result);
      }

      result.push({ key: cursor.primaryKey, value: cursor.value });
      cursor.continue();
    }

    request.onerror = event => {
      reject(event.target);
    }
  })
};

@Injectable()
export class IndexDbService {
  private dbIsReady;

  constructor() { }

  public openDb(config: Config): void {
    this.dbIsReady = new Promise((resolve, reject) => {
      let request = window.indexedDB.open(config.NAME, config.VERSION);
      handleRequest(request)
        .then((data: any) => {
          console.log('database opened successfully.');
          resolve(data.result);
        })
        .catch(() => {
          console.error('Unable to open the database!');
        })

      request.onupgradeneeded = event => {
        let target: any = event.target;
        let db = target.result;
        let objectStore = db.createObjectStore(
          config.STORE_NAME,
          { keyPath: config.KEYPATH, autoIncrement: config.KEYPATH_AUTO_INCREMENT || false }
        );

        if (config.INDEXES) {
          Object.keys(config.INDEXES).forEach(index => {
            objectStore.createIndex(
              config.INDEXES[index].indexName,
              config.INDEXES[index].keyPath,
              config.INDEXES[index].objectParameters || {}
            );
          })

        }
      }
    })
  }

  public read(objectName: string, config?: any): Promise<any> {
    return this.dbIsReady.then(db => {
      let objectStore = db.transaction(objectName)
        .objectStore(objectName);
      let range;

      if (config && config.index) {
        objectStore = objectStore.index(config.index);
      }

      if (config && config.range) {
        range = IDBKeyRange.bound(config.range.lower, config.range.upper);
      }

      return handleCollectionRequest(objectStore.openCursor(range));
    })
  }

  public readByIndex(objectName: string, index: string): Promise<any> {
    return this.read(objectName, {
      index: index
    });
  }

  public readByRangeAndIndex(objectName: string, index: string, lowerBound?: any, upperBound?: any): Promise<any> {
    return this.read(objectName, {
      index: index,
      range: {
        lower: lowerBound,
        upper: upperBound
      }
    });
  }

  public create(objectName: string, object: any): Promise<any> {
    return this.dbIsReady.then(db => {
      let request = db.transaction([objectName], 'readwrite')
        .objectStore(objectName)
        .add(object);
      return handleRequest(request);
    })
  }

  public update(objectName: string, keypPath: any, updater: (item: any) => any): Promise<any> {
    return this.dbIsReady.then(db => {
      let objectStore = db.transaction([objectName], 'readwrite')
        .objectStore(objectName);
      let request = objectStore.get(keypPath);
      let item;
      return handleRequest(request)
        .then((data: any) => {
          console.log('Item has been found in your database.');
          item = updater(data.result);
          return objectStore.put(item);
        })
        .then(handleRequest)
        .then(() => item)
    })
  }

  public delete(objectName: string, keypPath: any): Promise<any> {
    return this.dbIsReady.then(db => {
      let request = db.transaction([objectName], 'readwrite')
        .objectStore(objectName)
        .delete(keypPath);
      return handleRequest(request)
    })
  }

  public clearDb(objectName: string): Promise<any> {
    let request = window.indexedDB.deleteDatabase(objectName);
    return handleRequest(request);
  }

}
