
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule, Store, combineReducers } from '@ngrx/store';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { reducers } from '../../reducers/index';
import { ActiveTaskComponent } from './active-task.component';

describe('ActiveTaskComponent', () => {
	let component: ActiveTaskComponent;
	let fixture: ComponentFixture<ActiveTaskComponent>;
	let debugElement: DebugElement;
	let element: HTMLElement;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				StoreModule.forRoot(reducers)
			],
			declarations: [ActiveTaskComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ActiveTaskComponent);
		component = fixture.componentInstance;
		debugElement = fixture.debugElement.query(By.css('.active-task-id'));
		if (debugElement) {
			element = debugElement.nativeElement;
		}

		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});


});