export enum addTaskPopupText {
  TITLE = 'Create a new task',
  DETAIL_SENTENCE = 'Enter a task title, please',

};

export enum editTaskPopupText {
  TITLE = 'Edit a task',
  DETAIL_SENTENCE = 'Enter a new task title, please'
};

export enum TaskPopupModes {
  ADD_MODE = 'Add Mode',
  EDIT_MODE = 'Edit Mode'
};