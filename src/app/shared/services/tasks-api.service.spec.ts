import { TestBed, inject } from '@angular/core/testing';

import { IndexDbService as indexDb } from './index-db.service';
import { TasksApiService } from './tasks-api.service';
import { openDb } from './utils/utils';

let indexDbStub = {
	openDb: openDb
};

describe('TasksApiService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				[TasksApiService],
				{ provide: indexDb, useValue: indexDbStub }
			]
		});
	});

	it('should be created', inject([TasksApiService], (service: TasksApiService) => {
		expect(service).toBeTruthy();
	}));

});
