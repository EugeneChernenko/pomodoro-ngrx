import { Action } from '@ngrx/store';

export const ADD_MESSAGE = '[Notif] Add Message';
export const REMOVE_MESSAGE = '[Notif] Remove Message';

export class AddMessage implements Action {
	readonly type = ADD_MESSAGE;

	constructor(public payload: string) { }
}

export class RemoveMessage implements Action {
	readonly type = REMOVE_MESSAGE;
}

export type Actions
	= AddMessage
	| RemoveMessage;
